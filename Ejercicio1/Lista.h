#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        // Constructor.
        Lista();
        
        // Crea un nuevo nodo. 
        void crear (Numero *numero);
        // Imprime la lista. 
        void imprimir ();
};
#endif
