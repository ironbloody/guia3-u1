#include "Numero.h"

// Creacion de la estructura del nodo. 
typedef struct _Nodo {
    Numero *numero;
    struct _Nodo *sig;
} Nodo;
