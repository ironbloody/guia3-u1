**Guia 3 Unidad 1 Ejercicio 1**                                                                                             
Programa basado en programación orientada a objetos y listas enlazadas.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Problematica**                                                                                                                                       
Crear un programa que permita el ingreso de numeros a una lista enlazada, mostrando el la lista por cada ingreso.                                                         

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.                    
Una vez abierto el programa este le preguntara si desea ingresar un numero en la lista dandole la opcion 1 y 2. La opcion 1 le permitira añadir un numero a la lista y la opcion 2 cerrara el programa.                                                                            
                                                                                                                                                                                                                                             
**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:                                                                                                                               
Iostream                                                                                                                              

**Versionado**                                                                                                                                        
Version 1.6                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               




