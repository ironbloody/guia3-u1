#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {
    this -> raiz = raiz;
}

void Lista::crear (Numero *numero) {
    Nodo *tmp;

    // Se crea un nodo .
    tmp = new Nodo;
    // El nuevo nodo sera el numero.
    tmp->numero = numero;
    // El nodo apunta a NULL por defecto. 
    tmp->sig = NULL;

    // Si el es primer nodo de la lista, quedara como raíz y como último nodo. 
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    // Si no, apunta el último nodo al nuevo y deja el nuevo como el último de la lista. 
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}

void Lista::imprimir () {  
    // Se crea una variable temporal para recorrer la lista.
    Nodo *tmp = this->raiz;

    // Si temporal es distinto de NULL, se recorrera la lista.
    while (tmp != NULL) {
        cout << "[" << tmp->numero->get_numero() << "]";
        tmp = tmp->sig;
    }
    cout << endl;
}


