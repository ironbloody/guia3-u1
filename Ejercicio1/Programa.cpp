#include <iostream>
using namespace std;

// Definición de la estructura Nodo.
#include "Programa.h"

// Clases 
#include "Numero.h"
#include "Lista.h"


class Programa {
    private:
        Lista *lista = NULL;
    
    public:
        // Constructor 
        Programa() {
            this->lista = new Lista();
        }
    
        Lista *get_lista() {
            return this->lista;
        }
};

// Función principal.
int main (void) {
    // Creacion de variables
    int numero, opcion;
    Programa e = Programa();
    Lista *lista = e.get_lista();
    
    // Do para siempre preguntar si desea añadir un numero
    do {
        cout << "¿Desea ingresar un numero a la lista? 1=Si, 2=No" << endl;
        // Se guarda la opcion
        cin >> opcion;
        
        switch (opcion) {
            // Si la opcion es 1 pedira el ingreso del numero
            case 1:
                cout << "Ingrese el numero: " << endl;
                // Se guarda el numero ingresado
                cin >> numero; 
                
                Numero *numero1 = new Numero(numero);
                // Se creara el nuevo numero en la lista enlazada
                lista->crear(numero1);
                // Se imprime la lista
                lista->imprimir();
                break;
                
        } 
    // Si la opcion es 2, terminara el programa
    }while (opcion != 2);
    return 0;
}