#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
    private:
        int numero = 0;

    public:
        // Constructor.
       Numero(int numero);
        
        // Metodos Get and Set.
        int get_numero();
        void set_numero(int numero);
};
#endif
