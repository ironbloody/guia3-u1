#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear (Numero *numero) {
    Nodo *tmp;

    // Se crea un nodo.
    tmp = new Nodo;
    // El nuevo nodo sera el numero.
    tmp->numero = numero;
    // El nodo apunta a NULL por defecto.
    tmp->sig = NULL;

    // Si el es primer nodo de la lista, quedara como raíz y como último nodo.
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
        this->saisho = this->raiz;
    // Si no, apunta el último nodo al nuevo y deja el nuevo como el último de la lista. 
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}


void Lista::ordenar(Nodo *inicio){
    // Se crea un nodo siguiente
    Nodo *siguiente;
    int tmp;
    
    // mientras que el sig sea distinto de NULL.
    while(inicio -> sig != NULL){
        // El numero inicial sera el siguiente.
        siguiente = inicio -> sig;
        // Mientras que siguiente sea distitno de NULL.
        while(siguiente != NULL){
            // Condicion que el numero inicial sea mayor que el siguiente.
            if(inicio->numero->get_numero() > siguiente->numero->get_numero()){
                // tmp sera el numero siguiente.
                tmp = siguiente->numero->get_numero();
                // El numero siguiente se cambiara por el inicial.
                siguiente->numero->set_numero(inicio->numero->get_numero());
                // Y el inicial por el tmp.
                inicio->numero->set_numero(tmp);
            }
            siguiente = siguiente -> sig;
        }
        inicio = inicio -> sig;
        siguiente = inicio ->sig;
    }
}

void Lista::rellenar(int &tamano){
    // Se crea variable temporal
    Nodo *tmp = this->raiz;
    Nodo *siguiente = this->raiz;
    // Contador
    int cont = 0;
    
    // Mientras que la variable temporal sea distinto de NULL
    while (tmp != NULL){
        // Condicion, que el contador sea distinto del tamano menos uno.
        if(cont!=tamano-1){
            siguiente -> numero->get_numero();
            siguiente = siguiente->sig;
            // Condicion que el temporal mas uno sea distinto del numero siguiente.
            if((tmp->numero->get_numero() + 1)!= siguiente->numero->get_numero()){
                for(int numero = tmp->numero->get_numero() + 1; numero<siguiente->numero->get_numero(); numero++){
                    // Se crean los numeros faltantes.
                    crear((new Numero(numero)));
                    tamano++;
                    cont++;
                }
            }
        }
        tmp = tmp->sig;
        cont++;
    }
}

void Lista::imprimir () {  
    // Se crea una variable temporal para recorrer la lista.
    Nodo *tmp = this->raiz;
    int tamano = 0;

    // Si temporal es distinto de NULL, se recorrera la lista.
    while (tmp != NULL) {
        cout << "[" << tmp->numero->get_numero() << "]";
        tmp = tmp->sig;
        tamano++;
    }
    cout << endl;
 
}

