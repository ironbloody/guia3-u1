#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;
    
    public:
        Nodo *saisho = NULL;
        // Constructor.
        Lista();
        
        // Crea un nuevo nodo.
        void crear (Numero *numero);
        // Imprime la lista.
        void imprimir ();
        // Ordena la lista.
        void ordenar(Nodo *inicio);
        // Rellena la lista.
        void rellenar(int &tamano);
};
#endif
