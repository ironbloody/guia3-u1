**Guia 3 Unidad 1 Ejercicio 3**                                                                                             
Programa basado en programación orientada a objetos y listas enlazadas.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Problematica**                                                                                                                                       
Crear un programa que lea una lista enlazada de numeros enteros donde pueden existir valores no correlativos. El programa debe completar la lista con los numeros faltantes de la lista.                                                                                          
Ejemplo:                                                                                                                                  
Lista ingresada:
6-9-13      
Lista rellenada:
6-7-8-9-10-11-12-13                                   

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.                    
Una vez abierto el programa podra visualizar un menu con 3 opciones.

La primera opcion le permitira ingresar numeros a la lista, la segunda opcion rellenara la lista con los numeros faltantes y luego la imprimira. Finalmente la ultima opcion le permitira salir del programa.
                                                                                                                                                                                                                                             
**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:                                                                                                                               
Iostream                                                                                                                              

**Versionado**                                                                                                                                        
Version 1.12                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               




