#include <iostream>
using namespace std;

// Definición de la estructura Nodo.
#include "Programa.h"

// Clases.
#include "Numero.h"
#include "Lista.h"


class Ejemplo {
    private:
        Lista *lista = NULL;

    public:
        // Constructor.
        Ejemplo() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};

// Función main.
int main (void) {
    // Creacion de variables.
    int numero, opcion;
    int tamano = 0;
    Ejemplo e = Ejemplo();
    Lista *lista1 = e.get_lista();

    // Menu.
    do {
        cout << "------------------------------------------------" << endl;
        cout << "1. Ingresar un numero a la lista" << endl;
        cout << "2. Rellenar lista" << endl;
        cout << "3. Salir" << endl;
        cout << "------------------------------------------------" << endl;
        
        // Ingreso de opcion.
        cout << "Ingrese la opcion: ";
        // Se guarda la opcion.
        cin >> opcion;
        
        switch (opcion) {
            // Opcion 1.
            case 1:{
                // Ingreso de numero.
                cout << "Ingrese el numero: " << endl;
                // Se guarda el numero.
                cin >> numero; 
                // cada vez que se añade un numero, el tamaño aumenta.
                tamano++;
                Numero *numero1 = new Numero(numero);
                // Creacion del numero en la lista.
                lista1->crear(numero1);
                // Se ordena la lista.
                lista1->ordenar(lista1->saisho);
                // Se imprime la lista.
                lista1->imprimir();
                break;
                }
            // Opcion 2.
            case 2:{
                // Se rellena la lista con los numeros faltantes.
                lista1->rellenar(tamano);
                // Se ordena la lista.
                lista1->ordenar(lista1->saisho);
                // Impresion de la lista.
                lista1->imprimir();
                break;
                }
                
                
            }
        //Opcion 3 cierra el programa.
        }while (opcion != 3);
    return 0;
}