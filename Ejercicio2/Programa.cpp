#include <iostream>
using namespace std;

// Definición de la estructura Nodo.
#include "Programa.h"

// Clases.
#include "Numero.h"
#include "Lista.h"


class Ejemplo {
    private:
        Lista *lista = NULL;

    public:
        // Constructor.
        Ejemplo() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};

// Función main.
int main (void) {
    // Creacion de variables.
    int numero, opcion;
    // Creacion de 3 listas distintas.
    Ejemplo e = Ejemplo();
    Ejemplo e2 = Ejemplo();
    Ejemplo e3 = Ejemplo();
    Lista *lista1 = e.get_lista();
    Lista *lista2 = e2.get_lista();
    Lista *lista3 = e3.get_lista();
    
    // Menu.
    do {
        cout << "------------------------------------------------" << endl;
        cout << "1. Ingresar un numero a la primera lista" << endl;
        cout << "2. Ingresar un numero a la segunda lista" << endl;
        cout << "3. Mostrar lista conjunta" << endl;
        cout << "4. Salir" << endl;
        cout << "------------------------------------------------" << endl;
        
        // Ingreso de opcion.
        cout << "Ingrese la opcion: ";
        // Se guarda la opcion.
        cin >> opcion;
        
        switch (opcion) {
            // Opcion 1.
            case 1:{
                // Se pide ingreso del numero.
                cout << "Ingrese el numero: " << endl;
                // Se guarda el numero.
                cin >> numero; 
                Numero *numero1 = new Numero(numero);
                // Creacion del numero en la lista.
                lista1->crear(numero1);
                // Impresion de la lista.
                lista1->imprimir();
                // Creacion del numero en la lista conjunta.
                lista3->crear(numero1);
                break;
                }
            
            // Opcion 2.
            case 2:{
                // Se pide ingreso del numero.
                cout << "Ingrese el numero: " << endl;
                // Se guarda el numero.
                cin >> numero; 
                Numero *numero2 = new Numero(numero);
                // Creacion del numero en la lista.
                lista2->crear(numero2);
                // Impresion de la lista.
                lista2->imprimir();
                // Creacion del numero en la lista conjunta.
                lista3 ->crear(numero2);

                break;
                }
                
            // Opcion 3.
            case 3:{
                // Impresion lista conjunta.
                lista3->imprimir();
                break;
                }
                
            }
        // Opcion 4 cerrara el programa.
        }while (opcion != 4);
    return 0;
}