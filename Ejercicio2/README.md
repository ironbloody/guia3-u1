**Guia 3 Unidad 1 Ejercicio 2**                                                                                             
Programa basado en programación orientada a objetos y listas enlazadas.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Problematica**                                                                                                                                       
Crear un programa que permita crear 2 listas enlazadas y forme una tercera con los elementos de ambas listas. Finalmente mostrando la tercera lista.                                            

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.                    
Una vez abierto el programa visualizara un menu con 4 opciones.

La primera opcion le permitira ingresar numeros a la primera lista, la segunda opcion le permitira ingresar numeros a la segunda lista, la tercera opcion le mostrara la tercera lista con los elementos de las listas rellenadas anteriormente. Finalmente la ultima opcion le permitira salir del programa.
                                                                                                                                                                                                                                             
**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:                                                                                                                               
Iostream                                                                                                                              

**Versionado**                                                                                                                                        
Version 1.2                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               




