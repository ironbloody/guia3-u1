#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
    private:
        int numero = 0;

    public:
        // Constructor.
       Numero(int numero);
        
        // Metodo Get. 
        int get_numero();
};
#endif
